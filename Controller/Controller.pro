#-------------------------------------------------
#
# Project created by QtCreator 2020-06-23T14:56:24
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Controller
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    tcontroller.cpp \
    tchanelstatemultimeter.cpp

HEADERS  += mainwindow.h \
    tcontroller.h \
    tchanelstatemultimeter.h

FORMS    += mainwindow.ui
