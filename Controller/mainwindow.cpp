#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "tcontroller.h"
#include <qcombobox.h>
#include <QMessageBox>
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setWindowState(Qt::WindowMaximized);
    TController* iControler = TController::Hinstance();
    ui->TableChanelMultimetr->setRowCount(iControler->Chanels().size());
    for(int iRow=0;iRow<iControler->Chanels().size();iRow++)
    {
        QTableWidgetItem *iCell = NULL;
        iCell = new QTableWidgetItem;
        iCell->setText("Канал-"+QString::number(iRow));
        iCell->setFlags(Qt::ItemIsEnabled|Qt::ItemIsSelectable);
        ui->TableChanelMultimetr->setItem(iRow,0,iCell);

        iCell = new QTableWidgetItem;
        iCell->setText(iControler->Chanels().at(iRow)->Range());
        iCell->setFlags(Qt::ItemIsEnabled|Qt::ItemIsSelectable);
        ui->TableChanelMultimetr->setItem(iRow,1,iCell);

        iCell = new QTableWidgetItem;
        iCell->setText(iControler->Chanels().at(iRow)->Value());
        iCell->setFlags(Qt::ItemIsEnabled|Qt::ItemIsSelectable);
        ui->TableChanelMultimetr->setItem(iRow,3,iCell);

        iCell = new QTableWidgetItem;
        iCell->setText(iControler->Chanels().at(iRow)->State());
        iCell->setFlags(Qt::ItemIsEnabled|Qt::ItemIsSelectable);
        ui->TableChanelMultimetr->setItem(iRow,2,iCell);
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::resizeEvent(QResizeEvent *aValue)
{
    ui->TableChanelMultimetr->horizontalHeader()->setDefaultSectionSize((aValue->size().width()-10)/ui->TableChanelMultimetr->columnCount());
    if(150<ui->TableChanelMultimetr->horizontalHeader()->defaultSectionSize())
        ui->TableChanelMultimetr->setColumnWidth(0,150);
    //for(int i=0;i<ui->TableChanelMultimetr)
}

void MainWindow::on_TableChanelMultimetr_itemClicked(QTableWidgetItem *item)
{
    QComboBox* iCombobox = NULL;
    for(int iRow=0;iRow<ui->TableChanelMultimetr->rowCount();iRow++)
    {
        iCombobox = qobject_cast<QComboBox*>(ui->TableChanelMultimetr->cellWidget(iRow,1));
        if(iCombobox)
        {
            ui->TableChanelMultimetr->item(iRow,1)->setText(iCombobox->currentText());
            ui->TableChanelMultimetr->setCellWidget(iRow,1,NULL);
            break;
        }
    }
    if(item->column()==1)
    {
        TController* iControler = TController::Hinstance();
        iCombobox = new QComboBox;

        if(iControler->Chanels().at(item->row())->Range().isEmpty())
            iCombobox->addItem("Н/А");

        for(QVector<QString>::const_iterator iRange=iControler->Range().begin();iRange!=iControler->Range().end();iRange++)
            iCombobox->addItem(*iRange);
        ui->TableChanelMultimetr->setCellWidget(item->row(),item->column(),iCombobox);
        if(iControler->Chanels().at(item->row())->Range().isEmpty())
            iCombobox->setCurrentText("Н/А");
        else
            iCombobox->setCurrentText(iControler->Chanels().at(item->row())->Range());
        connect(iCombobox,SIGNAL(currentIndexChanged(int)),SLOT(on_change_range_chanel_multimeter(int)));



    }
}

void MainWindow::on_change_range_chanel_multimeter(int aIndex)
{

    TController* iControler = TController::Hinstance();
    QComboBox* iCombobox = NULL;
    for(int iRow=0;iRow<ui->TableChanelMultimetr->rowCount();iRow++)
    {
        iCombobox = qobject_cast<QComboBox*>(ui->TableChanelMultimetr->cellWidget(iRow,1));
        if(iCombobox)
        {
            aIndex -= (iControler->Range().size()<iCombobox->count())?1:0;
            if(iControler->ChangeRangeChanel(iRow,aIndex)==false)
            {
                ui->LabelServerSatus->setText("шибка информационного взаимодействия");
            }
            else
                 ui->LabelServerSatus->setText("Ok");
            break;
        }
    }
}

void MainWindow::on_aStart_Chanel_Measure_triggered()
{
    int cIndex = (unsigned)ui->TableChanelMultimetr->selectedItems().first()->row();
    TController* Icontroler = TController::Hinstance();
    if(Icontroler->StartMeasureChanel((unsigned)cIndex)==false)
    {
        ui->LabelServerSatus->setText("шибка информационного взаимодействия");
    }
    else
         ui->LabelServerSatus->setText("Ok");
    ui->TableChanelMultimetr->item(cIndex,2)->setText(Icontroler->Chanels().at(cIndex)->State());
}

void MainWindow::on_aStop_Chanel_Measure_triggered()
{
    int cIndex = (unsigned)ui->TableChanelMultimetr->selectedItems().first()->row();
    TController* Icontroler = TController::Hinstance();
    if(Icontroler->StopMeasureChanel((unsigned)cIndex)==false)
    {
        ui->LabelServerSatus->setText("шибка информационного взаимодействия");
    }
    else
         ui->LabelServerSatus->setText("Ok");

    ui->TableChanelMultimetr->item(cIndex,2)->setText(Icontroler->Chanels().at(cIndex)->State());
}

void MainWindow::on_aResult_Measure_Chanel_triggered()
{
    int cIndex = (unsigned)ui->TableChanelMultimetr->selectedItems().first()->row();
    TController* Icontroler = TController::Hinstance();
    if(Icontroler->ResultMeasureChanel((unsigned)cIndex)==false)
    {
        ui->LabelServerSatus->setText("шибка информационного взаимодействия");
    }
    else
    {
        ui->LabelServerSatus->setText("Ok");
        ui->TableChanelMultimetr->item(cIndex,3)->setText(Icontroler->Chanels().at(cIndex)->Value());
    }
    ui->TableChanelMultimetr->item(cIndex,2)->setText(Icontroler->Chanels().at(cIndex)->State());
}
