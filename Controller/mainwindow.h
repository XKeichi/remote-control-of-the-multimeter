#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QResizeEvent>
#include <QTableWidgetItem>
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
protected:
    void resizeEvent(QResizeEvent *aValue);
private slots:
    void on_TableChanelMultimetr_itemClicked(QTableWidgetItem *item);
    void on_change_range_chanel_multimeter(int aIndex);

    void on_aStart_Chanel_Measure_triggered();

    void on_aStop_Chanel_Measure_triggered();

    void on_aResult_Measure_Chanel_triggered();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
