#include "tchanelstatemultimeter.h"

typedef std::pair<int,int> pint;
TChanelStateMultimeter::TChanelStateMultimeter()
{
    FValue = "";
    FState = "";
    FRange = "";
}

void TChanelStateMultimeter::State(const std::string &aValue)
{
    QString iRow = QString::fromStdString(aValue).toLower().trimmed();
    if(iRow.left(2).trimmed()=="ok")
    {
        int iPosition=-1;
        if((iPosition=iRow.indexOf("range="))==-1)
            throw QString("Ошибка информационного взаимодействия");
        iRow = iRow.remove(0,iPosition+6).trimmed();
        if((iPosition=iRow.indexOf(","))==-1)
            throw QString("Ошибка информационного взаимодействия");
        FRange = iRow.left(iPosition).trimmed();
        if((iPosition=iRow.indexOf("status="))==-1)
            throw QString("Ошибка информационного взаимодействия");
        iRow = iRow.remove(0,iPosition+7).trimmed();
        if((iPosition=iRow.indexOf(","))==-1)
            throw QString("Ошибка информационного взаимодействия");
        QString dStatus = iRow.left(iPosition).trimmed();
        if((iPosition=iRow.indexOf("value="))==-1)
            throw QString("Ошибка информационного взаимодействия");
        iRow = iRow.remove(0,iPosition+6).trimmed();
        FValue = iRow;
        if(dStatus=="idle_state")
        {
            FState = "Готов";
            return;
        }
        if(dStatus=="measure_state")
        {
            FState = "Измерения";
            return;
        }
        if(dStatus=="busy_state")
        {
            FState = "Занят";
            return;
        }
        if(dStatus=="error_state")
        {
            FState = "Ошибка";
            return;
        }
        throw QString("Ошибка информационного взаимодействия");
    }

}

const QString TChanelStateMultimeter::State()const
{
    return FState;
}

const QString TChanelStateMultimeter::Range()const
{
    return FRange;
}

void TChanelStateMultimeter::Range(const QString &aValue)
{
    FRange = aValue;
}

const QString TChanelStateMultimeter::Value()const
{
    return FValue;
}
void TChanelStateMultimeter::Value(const QString &aValue)
{
    FValue = aValue;
}
