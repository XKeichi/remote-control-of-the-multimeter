#include "tcontroller.h"

const QString TController::DeviceSocetName = "/tmp/Socket/Multimeter.socket";
TController* TController::FHandleObjectController = NULL;

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <QDir>
#include <QFileInfo>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <string.h>

TController::TController()
{   
    FHandleSocketDevice = socket(AF_UNIX,SOCK_STREAM,0);
    sockaddr_un sName;
    memset(&sName,0x00,sizeof(struct sockaddr_un));
    sName.sun_family = AF_UNIX;
    strcpy(sName.sun_path,DeviceSocetName.toLocal8Bit().data());
    if(connect(FHandleSocketDevice,(struct sockaddr*)&sName,sizeof(struct sockaddr_un))==-1)
        throw QString("Программа управления прибором выгружена или не доступна");

    std::string iReqvest="";
    char Buffer[1024];
    memset(Buffer,0x00,1024);
    int rSize=0;
    while(memcmp(Buffer,"fail",4)!=0)
    {
        memset(Buffer,0x00,1024);
        iReqvest = QString("get_status chanel"+QString::number(FChanels.size())+"\r").toStdString();
        if(write(FHandleSocketDevice,iReqvest.data(),iReqvest.size())!=(qint64)iReqvest.size())
            throw QString("Ошибка сетевого взаимодействия");
        if(0<(rSize=read(FHandleSocketDevice,Buffer,1024)))
        {
            if(memcmp(Buffer,"ok",2)==0)
            {
                FChanels.push_back(new TChanelStateMultimeter);
                FChanels.last()->State(std::string(Buffer));
            }

        }
        else
            throw QString("Ошибка сетевого взаимодействия");

    }
    FRange.push_back("0.0000001-0.001");
    FRange.push_back("0.001-1");
    FRange.push_back("1-1000");
    FRange.push_back("1000-1000000");
}


TController::~TController()
{
    close(FHandleSocketDevice);
    FHandleObjectController = NULL;
}


TController* TController::Hinstance()
{
    if(FHandleObjectController==NULL)
        FHandleObjectController = new TController;
    return FHandleObjectController;
}

const QVector<TChanelStateMultimeter*>& TController::Chanels()const
{
    return FChanels;
}

const QVector<QString>& TController::Range()const
{
    return FRange;
}

bool TController::ChangeRangeChanel(unsigned int aChanel,unsigned int aRange)
{
    char Buffer[1024];
    memset(Buffer,0x00,1024);
    if(((qint64)aChanel<FChanels.size())&&((qint64)aRange<FRange.size()))
    {
        QString iComand = "set_range chanel"+QString::number(aChanel)+", range"+QString::number(aRange)+"\r";
        if(write(FHandleSocketDevice,iComand.toStdString().data(),iComand.toStdString().size())==(qint64)iComand.toStdString().size())
        {
            if(-1<read(FHandleSocketDevice,Buffer,1024))
            {
                if(memcmp(Buffer,"ok",2)==0)
                {
                    FChanels[(qint64)aChanel]->Range(FRange[(qint64)aRange]);
                    return true;
                }
            }
        }
    }
    return false;
}

void TController::CheckStatusChanel(unsigned int aIndex)
{
    char Buffer[1024];
    memset(Buffer,0x00,1024);
    QString iComand = QString("get_status chanel"+QString::number(aIndex)+"\r");
    if(write(FHandleSocketDevice,iComand.toStdString().data(),iComand.toStdString().size())!=(qint64)iComand.toStdString().size())
        throw QString("Ошибка сетевого взаимодействия");
    if(0<read(FHandleSocketDevice,Buffer,1024))
    {
        if(memcmp(Buffer,"ok",2)==0)
        {
            FChanels.at((qint64)aIndex)->State(std::string(Buffer));
            return;
        }

    }
    else
        throw QString("Ошибка сетевого взаимодействия");
    throw QString("Ошибка информационного взаимодействия");
}

bool TController::StartMeasureChanel(unsigned int aIndex)
{
    bool Result=false;
    char Buffer[1024];
    memset(Buffer,0x00,1024);
    if((qint64)aIndex<FChanels.size())
    {
        QString iComand = "start_measure chanel"+QString::number(aIndex)+"\r";
        if(write(FHandleSocketDevice,iComand.toStdString().data(),iComand.toStdString().size())==(qint64)iComand.toStdString().size())
        {
            if(-1<read(FHandleSocketDevice,Buffer,1024))
            {
                if(memcmp(Buffer,"ok",2)==0)
                {
                    Result = true;
                }
            }
        }
        CheckStatusChanel(aIndex);

    }
    return Result;
}

bool TController::StopMeasureChanel(unsigned int aIndex)
{
    bool Result=false;
    char Buffer[1024];
    memset(Buffer,0x00,1024);
    if((qint64)aIndex<FChanels.size())
    {
        QString iComand = "stop_measure chanel"+QString::number(aIndex)+"\r";
        if(write(FHandleSocketDevice,iComand.toStdString().data(),iComand.toStdString().size())==(qint64)iComand.toStdString().size())
        {
            if(-1<read(FHandleSocketDevice,Buffer,1024))
            {
                if(memcmp(Buffer,"ok",2)==0)
                {
                    Result = true;
                }
            }
        }
        CheckStatusChanel(aIndex);

    }
    return Result;
}

bool TController::ResultMeasureChanel(unsigned int aIndex)
{
    bool Result=false;
    char Buffer[1024];
    memset(Buffer,0x00,1024);
    if((qint64)aIndex<FChanels.size())
    {
        QString iComand = "get_result chanel"+QString::number(aIndex)+"\r";
        if(write(FHandleSocketDevice,iComand.toStdString().data(),iComand.toStdString().size())==(qint64)iComand.toStdString().size())
        {
            if(-1<read(FHandleSocketDevice,Buffer,1024))
            {
                if(memcmp(Buffer,"ok",2)==0)
                {
                    iComand = QString::fromLocal8Bit(Buffer);
                    while (-1<iComand.indexOf(","))
                        iComand = iComand.remove(0,iComand.indexOf(",")+1).trimmed();
                    FChanels[(qint64)aIndex]->Value(iComand);
                    Result = true;
                }
                CheckStatusChanel(aIndex);
            }
        }
    }
    return Result;
}
