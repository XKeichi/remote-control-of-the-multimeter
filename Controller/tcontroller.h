#ifndef TCONTROLLER_H
#define TCONTROLLER_H

#include<QObject>
#include<QPair>
#include<QVector>
#include<tchanelstatemultimeter.h>
class TController
{
public:
    static TController *Hinstance();
    virtual ~TController();   
    const QVector<TChanelStateMultimeter*>& Chanels()const;
    const QVector<QString>& Range()const;
    bool ChangeRangeChanel(unsigned int aChanel,unsigned int aRange);
    bool StartMeasureChanel(unsigned int aIndex);
    bool StopMeasureChanel(unsigned int aIndex);
    bool ResultMeasureChanel(unsigned int aIndex);
signals:
    void UpdateChanelStatus(const TChanelStateMultimeter *aChanel);
private:
    TController();
    void CheckStatusChanel(unsigned int aIndex);
private slots:
    void ReadStatusChanels();
private:
    int FHandleSocketDevice;    
    QVector<QString>FRange;
    QVector<TChanelStateMultimeter*>FChanels;
private:
    static TController* FHandleObjectController;
    static const QString DeviceSocetName;
};

#endif // TCONTROLLER_H
