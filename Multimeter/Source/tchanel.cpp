#include "tchanel.h"
#include "tmultimeter.h"
#include <sstream>
#include <iosfwd>
#include <iomanip>
#include <math.h>
const struct TChanel::SRangChanelValue TChanel::FRangChanelValue[4] = {
    0.0000001,           0.001,
    0.001,             1.f,
    1.f,          1000.f,
    1000.f,       1000000.f
};

const std::map<std::string,TChanel::TPProcedure>TChanel::FControls = { make_pair("start_measure",&TChanel::start_measure),
                                                                       make_pair("set_range",&TChanel::set_range),
                                                                       make_pair("stop_measure",&TChanel::stop_measure),
                                                                       make_pair("get_status",&TChanel::get_status),
                                                                       make_pair("get_result",&TChanel::get_result) };

using namespace std;
TChanel::TChanel(TMultimeter *aOwner):FOwner(aOwner),FState(idle_state), FOnFinish(NULL)
{
    FSuccsess = ok;
    FChanelSetRange = nullptr;
}
TChanel::~TChanel()
{
    if(FOnFinish)
        delete FOnFinish;
    if(FChanelSetRange)
        delete FChanelSetRange;
}

const string TChanel::Result()const
{
    return (FSuccsess==ok)?"ok":"fail";
}

const string TChanel::Range()const
{
    if(FChanelSetRange==NULL)
        return "0-0";
    std::ostringstream os;
    double rem[] = {FChanelSetRange->Min-(int64_t)FChanelSetRange->Min, FChanelSetRange->Max-(int64_t)FChanelSetRange->Max};
    int prec[] = {rem[0]==0?0:(int)-log10(rem[0])+1,rem[1]==0?0:(int)-log10(rem[1])+1};
    os<<std::fixed<<std::setprecision(prec[0])<<FChanelSetRange->Min<<"-"<<std::setprecision(prec[1])<<FChanelSetRange->Max;
    string rValue = os.str();
    return rValue;
}
const string TChanel::Value()const
{
    return FValue;
}
const std::string TChanel::Status()const
{
    switch(FState)
    {
    case idle_state:
        return "idle_state";
        break;
    case measure_state:
        return "measure_state";
        break;
    case busy_state:
        return "busy_state";
        break;
    case error_state:
        return "error_state";
        break;
    default:;
    }
    return "undefine";
}

bool TChanel::Controll(const std::vector<std::string> &aComand)
{
    if(FMutex.try_lock())//Синхронизация:
                         //позволяем выполнять по 1 команде на каждом канале прибора
                         //сброс в мютекса в теле потока
    {
        thread ExecuteProcessComand(TChanel::MainTreadFunction,this,aComand);
        ExecuteProcessComand.detach();//Отпускаем поток
        return true;
    }
    return false;
}


TChanel::TSuccsess TChanel::start_measure(const std::vector<std::string> &aComand)
{
    (void)aComand;
    if((FState == idle_state)&&(FChanelSetRange))//разрешаем измерять значение канала после его установки диаппазона
    {
        FState = measure_state;
        FSuccsess =  ok;
        return FSuccsess;
    }
    FSuccsess =  fail;
    return FSuccsess;
}

TChanel::TSuccsess TChanel::set_range(const std::vector<std::string> &aComand)
{
    if((FState==idle_state)&&(aComand.size()==3))//Разрешаем изменять диапазон при выключеном канале (эмитация работы прибора)
    {                                            //также проверяем валидность синтаксиса команды

        size_t Index = aComand[2].find("range");
        if(Index!=string::npos)//завершающия проверка синтаксиса
        {
            Index = atoi(aComand[2].data()+Index+5);
            if(Index<4)
            {
                FChanelSetRange = const_cast<struct SRangChanelValue*>(&FRangChanelValue[Index]);
                FSuccsess =  ok;
                return FSuccsess;
            }

        }


    }
    FSuccsess =  fail;
    return FSuccsess;
}

TChanel::TSuccsess TChanel::stop_measure(const std::vector<std::string> &aComand)
{
    (void)aComand;
    if(idle_state<FState)
    {
        FState = idle_state;
        FSuccsess =  ok;
        return FSuccsess;
    }
    FSuccsess =  fail;
    return FSuccsess;
}

TChanel::TSuccsess TChanel::get_status(const std::vector<std::string> &aComand)
{
    (void)aComand;
    FSuccsess =  ok;
    return FSuccsess;
}

TChanel::TSuccsess TChanel::get_result(const std::vector<std::string> &aComand)
{
    if(aComand.size()==2)
    {
        if(idle_state<FState)
        {
            if((rand()%100)<20)//емитируем ошибку
            {
                FState = error_state;
                FSuccsess = fail;
                return FSuccsess;
            }
            FState = busy_state;//эмитируем работу прибора
            std::this_thread::sleep_for(std::chrono::seconds(rand()%5+1));
            const int lv[2] = { (int)log10(FChanelSetRange->Min), (int)log10(FChanelSetRange->Max) };
            const double rndvalue = double(rand()%((unsigned int)pow(10,lv[1]-lv[0])))*FChanelSetRange->Min;
            double rem = rndvalue-(int)rndvalue;
            int percision = (rem==0)?0:((int)-log(rem)+1);
            std::ostringstream os;
            os<<std::fixed<<std::setprecision(percision)<<rndvalue;
            FValue = os.str();


            //FValue = to_string(rndvalue);
            FState = measure_state;
            FSuccsess = ok;
            return FSuccsess;

        }

    }
    FSuccsess = fail;
    return FSuccsess;
}

TChanel::TSuccsess TChanel::Execute(const std::vector<std::string> &aComand)
{
    auto iComand = FControls.find(*aComand.begin());
    if(iComand!=FControls.end())//проверка синтаксиса
    {
        return (this->*iComand->second)(aComand);
    }
    FSuccsess = fail;
    return FSuccsess;
}

void TChanel::MainTreadFunction(TChanel* aOwner, const std::vector<std::string> &aComand)
{
    aOwner->Execute(aComand);
    aOwner->FMutex.unlock();
    if(aOwner->FOnFinish)
        aOwner->FOnFinish->Emit();
}
