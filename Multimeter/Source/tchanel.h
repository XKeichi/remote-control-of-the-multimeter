#ifndef TCHANEL_H
#define TCHANEL_H
#include <map>
#include <string>
#include <vector>
#include "tevent.h"
#include <mutex>
#include <thread>
class TMultimeter;



class TChanel
{

public:
    TChanel(TMultimeter *aOwner);
    virtual ~TChanel();
    bool Controll(const std::vector<std::string> &aComand);
    const std::string Range()const;
    const std::string Value()const;
    const std::string Status()const;
    const std::string Result()const;
    template<typename TRecived>void OnComplited(TRecived *aRecived, void(TRecived::*aSlot)(const TChanel*));
protected:
    enum TState {idle_state,measure_state,busy_state, error_state};
    enum TSuccsess{ok,fail};
protected:
    virtual TSuccsess Execute(const std::vector<std::string> &aComand);
private:
    struct SRangChanelValue;
    typedef TSuccsess(TChanel::*TPProcedure)(const std::vector<std::string>&);
private:
    const static std::map<std::string,TPProcedure>FControls;
    static const struct SRangChanelValue FRangChanelValue[4];
    TMultimeter *FOwner;
    TState FState;
    TEmiter* FOnFinish;
    std::string FValue;
    TSuccsess FSuccsess;
    struct SRangChanelValue *FChanelSetRange;
    std::mutex FMutex;
private:    
    TSuccsess start_measure(const std::vector<std::string> &aComand);
    TSuccsess set_range(const std::vector<std::string> &aComand);
    TSuccsess stop_measure(const std::vector<std::string> &aComand);
    TSuccsess get_status(const std::vector<std::string> &aComand);
    TSuccsess get_result(const std::vector<std::string> &aComand);
    static void MainTreadFunction(TChanel* aOwner, const std::vector<std::string> &aComand);

};

struct TChanel::SRangChanelValue
{
    float Min;
    float Max;
};

template<typename TRecived>void TChanel::OnComplited(TRecived *aRecived, void(TRecived::*aSlot)(const TChanel*))
{
    TEvent<TChanel,TRecived>*iSignal =  new TEvent<TChanel,TRecived>(this);
    iSignal->Connect(aRecived,aSlot);
    FOnFinish = iSignal;
}

#endif // TCHANEL_H
