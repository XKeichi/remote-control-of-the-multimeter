#include "tmultimeter.h"

const unsigned int TConnection::FSizeBufferMessage = 1024;

#include <unistd.h>
#include <memory.h>
#include <algorithm>
using namespace std;

TConnection::TConnection(int aHandle):FHandle(aHandle)
{

    FisReading = false;
    FLoop.store(true);
    FThread = NULL;
}

TConnection::~TConnection()
{
    if(FOnReadComand)
    {
        delete FOnReadComand;
        FOnReadComand = nullptr;
    }
    FLoop.store(false);
    FThread->join();
    delete FThread;
    FThread = nullptr;
}

void TConnection::run()
{
    char Buffer[FSizeBufferMessage];   
    int SizeSocketRead = 0;    
    string BildingCommandBuffer;
    string::iterator pEndComand;
    while(FLoop.load())
    {
        memset(Buffer,0x00,FSizeBufferMessage);
        if(0<(SizeSocketRead=read(FHandle,Buffer,FSizeBufferMessage)))
        {            
            BildingCommandBuffer+=Buffer;
            if((pEndComand = find(BildingCommandBuffer.begin(),BildingCommandBuffer.end(),'\r'))!=BildingCommandBuffer.end())
            {
                auto iBegin = BildingCommandBuffer.begin();
                auto iEnd = pEndComand;
                vector<string>iComand;
                while((iEnd=find(iBegin,pEndComand,' '))!=pEndComand)
                {
                    string iString(iBegin,iEnd);
                    iBegin = iEnd+1;
                    iComand.push_back(iString);

                }
                string iString(iBegin,iEnd);
                iComand.push_back(iString);

                BildingCommandBuffer.erase(BildingCommandBuffer.begin(),pEndComand+1);
                pthread_mutex_lock(&FMutex);
                FMessage.push_back(iComand);
                pthread_mutex_unlock(&FMutex);

                if(FOnReadComand)
                    FOnReadComand->Emit();

            }

        }
        std::this_thread::yield();
    }
}

void TConnection::Thread(TConnection *aHandel)
{
    aHandel->run();
}

int TConnection::ID()const
{
    return FHandle;
}

bool TConnection::Message(std::vector<std::string> &rMessage)
{
    pthread_mutex_lock(&FMutex);
    if(!FMessage.empty())
    {
        rMessage = *(FMessage.begin());
        FMessage.erase(FMessage.begin());
        pthread_mutex_unlock(&FMutex);
        return true;
    }
    pthread_mutex_unlock(&FMutex);
    return false;

}

bool TConnection::StartReadLoop()
{
    if(FOnReadComand)
    {
        FThread = new std::thread(TConnection::Thread,this);
        return true;
    }
    return false;
}
