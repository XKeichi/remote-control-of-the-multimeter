#ifndef TCONNECTION_H
#define TCONNECTION_H

#include<vector>
#include<string>
#include<thread>
#include<atomic>

#include "tevent.h"
class TConnection
{

public:
    explicit TConnection(int aHandle);
    virtual ~TConnection();
    template<typename TRecived>void OnReadComand(TRecived *aRecived, void(TRecived::*aSlot)(const TConnection*));
    int ID()const;
    bool StartReadLoop();
    bool Message(std::vector<std::string> &rMessage);
private:

    const int FHandle;
    std::vector<std::vector<std::string> >FMessage;
    static const unsigned int FSizeBufferMessage;
    bool FisReading;
    pthread_mutex_t FMutex;
    std::thread *FThread;
    void* FReciverObjectSlot;

    std::atomic<bool>FLoop;

    TEmiter* FOnReadComand;

private:
    void run();
    static void Thread(TConnection *aHandel);

};



template<typename TRecived>void TConnection::OnReadComand(TRecived *aRecived, void(TRecived::*aSlot)(const TConnection*))
{
    TEvent<TConnection,TRecived>*iSignal =  new TEvent<TConnection,TRecived>(this);
    iSignal->Connect(aRecived,aSlot);
    FOnReadComand = iSignal;
}

#endif // TCONNECTION_H
