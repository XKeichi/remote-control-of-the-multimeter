#ifndef TEVENT_H
#define TEVENT_H

class TEmiter
{
public:
    virtual bool Emit()=0;
    virtual ~TEmiter(){}
};

template <typename TSender, typename TReciver>
class TEvent : public TEmiter
{
public:
    TEvent(TSender* aSender);
    void Connect(TReciver* aReciver, void(TReciver::*aSlot)(const TSender *aSender));
    void SetReciver(TReciver* aReciver);
    void SetSlot(void(TReciver::*aSlot)(const TSender*));
    bool Emit()override final;

private:
    typedef void(TReciver::*TpSlor)(const TSender*);
private:
    TSender * FSender;
    TReciver* FReciver;
    TpSlor    FSlot;

};

template<typename TSender,typename TReciver>TEvent<TSender,TReciver>::TEvent(TSender* aSender):
    FSender(aSender),FReciver(nullptr),FSlot(nullptr)
{

}

template<typename TSender,typename TReciver>void TEvent<TSender,TReciver>::Connect(TReciver* aReciver, void(TReciver::*aSlot)(const TSender*))
{
    FReciver = aReciver;
    FSlot = aSlot;
}

template<typename TSender,typename TReciver>void TEvent<TSender,TReciver>::SetReciver(TReciver* aReciver)
{
    FReciver = aReciver;
}

template<typename TSender,typename TReciver>void TEvent<TSender,TReciver>::SetSlot(void(TReciver::*aSlot)(const TSender*))
{
    FSlot = aSlot;
}

template<typename TSender,typename TReciver>bool TEvent<TSender,TReciver>::Emit()
{
    if((FReciver)&&(FSlot))
    {
        (FReciver->*FSlot)(FSender);
        return true;
    }
    return false;
}

#endif // TEVENT_H
