#include "tmultimeter.h"
#include <memory.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <unistd.h>


const unsigned int TMultimeter::FCountChanelMultimeter = 5+rand()%5;

using namespace std;

__inline bool ExtractArgumentIndex(string aArgument, string aName, unsigned int *rValue)
{
    if(memcmp(aArgument.data(),aName.data(),aName.size())==0)
    {
        *rValue = atoi(aArgument.data()+aName.size());
        return true;
    }
    return false;
}

TMultimeter::TMultimeter()
{



    FChanels.resize(FCountChanelMultimeter,nullptr);
    for(TChanel*& iChanle:FChanels)
    {
        iChanle = new TChanel(this);
        iChanle->OnComplited<TMultimeter>(this,&TMultimeter::SendResultClient);
    }

}

TMultimeter::~TMultimeter()
{
   for_each(FChanels.begin(),FChanels.end(),[](TChanel*&iChanel){delete iChanel;iChanel=nullptr;});
   FChanels.clear();
}


bool TMultimeter::Controll(const std::vector<std::string> &aComand)
{
    if(1<aComand.size())
    {
        size_t Index = aComand[1].find("chanel");
        if(Index!=string::npos)
        {
            Index = atoi(aComand[1].data()+6);
            if(Index<FChanels.size())
            {
                return FChanels[Index]->Controll(aComand);
            }

        }
    }

    return false;
}

void TMultimeter::SendResultClient(const TChanel* aSender)
{    
    string Result = aSender->Result();
    Result+=":chanel="+std::to_string(int(find(FChanels.begin(),FChanels.end(),aSender)-FChanels.begin()));
    Result+=",range="+aSender->Range();
    Result+=",status="+aSender->Status();
    Result+=",value="+aSender->Value();
    SendAnswerComand(Result);

}
