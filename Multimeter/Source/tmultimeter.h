#ifndef TMULTIMETER_H
#define TMULTIMETER_H

#include "tnetwork.h"
#include <map>
#include <algorithm>
#include "tchanel.h"

#include <list>

class TMultimeter : public TNetwork
{

public:
    TMultimeter();
    virtual ~TMultimeter();
protected:
    virtual bool Controll(const std::vector<std::string> &aComand);
private:

    std::vector<TChanel*>FChanels;
    static const unsigned int FCountChanelMultimeter;
private:

    void SendResultClient(const TChanel *aSender);


};

#endif // TMULTIMETER_H
