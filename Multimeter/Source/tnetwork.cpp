#include "tnetwork.h"

const char TNetwork::DeviceSocetName[] = "/tmp/Socket/Multimeter.socket";

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <fcntl.h>

#include <algorithm>

#include <libgen.h>


using namespace std;

#include <dirent.h>


#include <signal.h>

TNetwork::TNetwork()
{

    string iDir = "/tmp/Socket";
    if(opendir(iDir.data())==NULL)
        mkdir(iDir.data(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    FHandleSocketDevice = socket(AF_UNIX,SOCK_STREAM,0);
    struct stat buffer;
    if(stat (DeviceSocetName, &buffer) == 0)
        unlink(DeviceSocetName);
    if(FHandleSocketDevice==-1)
        throw string("Ошибка: сокет закрыт");
    struct sockaddr_un sName;
    memset(&sName,0x00,sizeof(struct sockaddr_un));
    sName.sun_family = AF_UNIX;
    strcpy(sName.sun_path,DeviceSocetName);
    if(bind(FHandleSocketDevice,(struct sockaddr*)&sName,sizeof(struct sockaddr_un))==-1)
    {
        throw string("Ошибка:Bind()");
    }
    int Flags = fcntl(FHandleSocketDevice,F_GETFL,0);
    if(fcntl(FHandleSocketDevice,F_SETFL,Flags|O_NONBLOCK)<0)
        throw string("Ошибка настройки сокета");
    FWaiteSendMessages.store(false);

}

TNetwork::~TNetwork()
{    
    for(auto &iCLient:FClients)
    {
        close(iCLient->ID());
        delete iCLient;
        iCLient = nullptr;
    }
    FClients.clear();
    unlink(DeviceSocetName);
    close(FHandleSocketDevice);

}

void TNetwork::Start()
{
    int data_socket=0;
    if(-1<listen(FHandleSocketDevice,20))
    {
        while(true)
        {
            if(-1<(data_socket = accept(FHandleSocketDevice,NULL,NULL)))
            {
                TConnection* iSender = new TConnection(data_socket);
                FClients.push_back(iSender);
                iSender->OnReadComand<TNetwork>(this,&TNetwork::ReadClientMessage);
                iSender->StartReadLoop();

            }
            if(FMutexDeleteDisconectedClients.try_lock())
            {
                if(0<FPointers_Disconnected_Clients.size())
                {
                    FWaiteSendMessages.store(true);
                    for(auto iClient : FPointers_Disconnected_Clients)
                    {
                        auto iterator_delete_clients = find(FClients.begin(),FClients.end(),iClient);
                        if(iterator_delete_clients!=FClients.end())
                        {
                            delete *iterator_delete_clients;
                            *iterator_delete_clients = nullptr;
                            FClients.erase(iterator_delete_clients);
                        }
                    }
                    FWaiteSendMessages.store(false);

                }
                FMutexDeleteDisconectedClients.unlock();
            }

        }

    }
    else
        throw string("Ошибка: отказ функции listen");    
}

void TNetwork::ReadClientMessage(const TConnection *aSender)
{
    TConnection *iClient = const_cast<TConnection*>(aSender);
    std::vector<string>Command;

    iClient->Message(Command);
    for(auto iField : Command)
    {
        if(iField.size()==0)
        {
            string rValue = "fail, invalid comand";
            write(aSender->ID(),rValue.data(),rValue.size());
            return;
        }
        std::transform(iField.begin(),iField.end(),iField.begin(),
                       [](char c){return ::tolower(c);});
    }

    if(Controll(Command)==false)
    {
        string rValue = "fail, executing";
        write(aSender->ID(),rValue.data(),rValue.size());
        return;
    }

}

void TNetwork::SendAnswerComand(const string aValue)
{
    while(FWaiteSendMessages.load());
    for(auto iClient : FClients)
    {
        if(send(iClient->ID(),aValue.data(),aValue.size(),MSG_NOSIGNAL)==-1)//нашли отвалившейся клиент
        {
            std::lock_guard<std::mutex>lock(FMutexDeleteDisconectedClients);
            if(find(FPointers_Disconnected_Clients.begin(),FPointers_Disconnected_Clients.end(),iClient)==FPointers_Disconnected_Clients.end())
                FPointers_Disconnected_Clients.push_back(iClient);
        }
    }
}
