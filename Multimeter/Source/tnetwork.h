#ifndef TNETWORK_H
#define TNETWORK_H

#include "tconnection.h"
#include <mutex>
class TNetwork
{

public:
    explicit TNetwork();
    virtual ~TNetwork();
    void Start();

protected:
    virtual bool Controll(const std::vector<std::string> &aComand)=0;
    void SendAnswerComand(const std::string aValue);
private:
    static const char DeviceSocetName[];
    int FHandleSocketDevice;
    std::vector<TConnection*>FClients;
    std::vector<TConnection*>FPointers_Disconnected_Clients;
    std::mutex FMutexDeleteDisconectedClients;
    std::atomic<bool>FWaiteSendMessages;
private:
    void ReadClientMessage(const TConnection *aClient);
};



#endif // TNETWORK_H
