SOURCES +=\
    $$PWD/tchanelstatemultimeter.cpp\
    $$PWD/tcontroller.cpp \
    Controller/tmultimeter_chanel_model.cpp \
    Controller/trange_chanel_multimeter.cpp


HEADERS +=\
    $$PWD/tchanelstatemultimeter.h\
    $$PWD/tcontroller.h \
    Controller/tmultimeter_chanel_model.h \
    Controller/trange_chanel_multimeter.h


INCLUDEPATH += $$PWD
DEPENDPATH  += $$PWD
