#ifndef TCHANELSTATEMULTIMETER_H
#define TCHANELSTATEMULTIMETER_H

#include <QString>

class TChanelStateMultimeter
{
public:
    TChanelStateMultimeter();
    void State(const std::string &aValue);
    const QString State()const;
    const QString Range()const;
    void Range(const QString &aValue);
    const QString Value()const;
    void Value(const QString &aValue);
private:
    QString FValue;
    QString FRange;
    QString FState;

};

#endif // TCHANELSTATEMULTIMETER_H
