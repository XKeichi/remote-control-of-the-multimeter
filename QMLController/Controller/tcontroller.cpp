#include "tcontroller.h"

const QString TController::DeviceSocetName = "/tmp/Socket/Multimeter.socket";
TController* TController::FHandleObjectController = NULL;

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <QDir>
#include <QFileInfo>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <fcntl.h>
#include <string.h>
#include <QDebug>
TController::TController()
{       
    FHandleSocketDevice = socket(AF_UNIX,SOCK_STREAM,0);
    int Flags = fcntl(FHandleSocketDevice,F_GETFL,0);
    if(fcntl(FHandleSocketDevice,F_SETFL,Flags|O_NONBLOCK)<0)
        throw QString("Ошибка настройки сокета");
    sockaddr_un sName;
    memset(&sName,0x00,sizeof(struct sockaddr_un));
    sName.sun_family = AF_UNIX;
    strcpy(sName.sun_path,DeviceSocetName.toLocal8Bit().data());
    if(::connect(FHandleSocketDevice,(struct sockaddr*)&sName,sizeof(struct sockaddr_un))==-1)
        throw QString("Программа управления прибором выгружена или не доступна");

    std::string iReqvest="";
    char Buffer[1024];
    memset(Buffer,0x00,1024);
    int rSize=0;
    while(memcmp(Buffer,"fail",4)!=0)
    {
        memset(Buffer,0x00,1024);
        iReqvest = QString("get_status chanel"+QString::number(FChanels.Chanels().size())+"\r").toStdString();
        write(FHandleSocketDevice,iReqvest.data(),iReqvest.size());

        while ((rSize=read(FHandleSocketDevice,Buffer,1024))<1);
        if(memcmp(Buffer,"ok",2)==0)
        {
            FChanels.Chanels().push_back(new TChanelStateMultimeter);
            FChanels.Chanels().last()->State(std::string(Buffer));
        }

    }

    FRange.push_back("0.0000001-0.001");
    FRange.push_back("0.001-1");
    FRange.push_back("1-1000");
    FRange.push_back("1000-1000000");
    FClothReadThread = false;
    moveToThread(&FThreadReadStatus);
    connect(&FThreadReadStatus,SIGNAL(started()),SLOT(ReadStateChanels()));
    FThreadReadStatus.start();
}

TController::~TController()
{
    FMutex.lock();
    FClothReadThread = true;
    FMutex.unlock();
    while (FThreadReadStatus.isRunning())
        FThreadReadStatus.wait();
    close(FHandleSocketDevice);
    FHandleObjectController = NULL;
}


TController* TController::Hinstance()
{
    if(FHandleObjectController==NULL)
        FHandleObjectController = new TController;
    return FHandleObjectController;
}



TMultimeter_Chanel_Model* TController::getChanels()
{
    return &FChanels;
}

TRange_Chanel_Multimeter *TController::getRanges()
{
    return &FRange;
}

bool TController::changeRangeChanel(unsigned int aChanel,unsigned int aRange)
{

    if(((qint64)aChanel<FChanels.Chanels().size())&&((qint64)aRange<FRange.size()))
    {
        QString iComand = "set_range chanel"+QString::number(aChanel)+", range"+QString::number(aRange)+"\r";
        write(FHandleSocketDevice,iComand.toStdString().data(),iComand.toStdString().size());
        return true;
    }
    return false;
}

bool TController::startMeasureChanel(unsigned int aIndex)
{

    if((qint64)aIndex<FChanels.Chanels().size())
    {
        QString iComand = "start_measure chanel"+QString::number(aIndex)+"\r";
        write(FHandleSocketDevice,iComand.toStdString().data(),iComand.toStdString().size());
        return true;

    }

    return false;
}

bool TController::stopMeasureChanel(unsigned int aIndex)
{

    if((qint64)aIndex<FChanels.Chanels().size())
    {
        QString iComand = "stop_measure chanel"+QString::number(aIndex)+"\r";
        write(FHandleSocketDevice,iComand.toStdString().data(),iComand.toStdString().size());
        return true;


    }
    return false;
}

bool TController::resultMeasureChanel(unsigned int aIndex)
{

    if((qint64)aIndex<FChanels.Chanels().size())
    {
        QString iComand = "get_result chanel"+QString::number(aIndex)+"\r";
        write(FHandleSocketDevice,iComand.toStdString().data(),iComand.toStdString().size());
        return true;
    }
    return false;
}

void TController::updateDisplayValue()
{
    FChanels.updateDisplayValue();
}

void TController::ReadStateChanels()
{
    bool iLoop = true;
    char Buffer[1024];
    while (iLoop)
    {
        memset(Buffer,0x00,1024);
        if(-1<read(FHandleSocketDevice,Buffer,1024))
        {
            QString iComand = QString::fromLocal8Bit(Buffer).toLower().trimmed();
            int sPosition = iComand.indexOf("chanel=");
            int ePosition = iComand.indexOf(",",sPosition);
            if((-1<sPosition)&&(-1<ePosition))
            {
                bool Ok;
                int Index = iComand.mid(sPosition+7,ePosition-sPosition-7).trimmed().toInt(&Ok);
                if(Ok)
                {
                    if(Index<FChanels.Chanels().size())
                    {
                        FChanels.Chanels().at(Index)->State(Buffer);
                        FChanels.updateDisplayValue((unsigned int*)&Index);
                    }
                }
            }
        }
        if(FMutex.tryLock())
        {
            iLoop = !FClothReadThread;
            FMutex.unlock();
        }
    }
    FThreadReadStatus.exit(0);

}
