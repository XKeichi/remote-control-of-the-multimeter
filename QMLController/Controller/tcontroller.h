#ifndef TCONTROLLER_H
#define TCONTROLLER_H

#include <QObject>
#include <QPair>
#include <QVector>
#include "tchanelstatemultimeter.h"
#include "tmultimeter_chanel_model.h"
#include "trange_chanel_multimeter.h"
#include <QQmlListProperty>
#include <QStringList>
#include <QMutex>
#include <QThread>
typedef QAbstractListModel* pTModel;
class TController : public QObject
{
    Q_OBJECT
    Q_PROPERTY(TMultimeter_Chanel_Model* chanels READ getChanels CONSTANT)
    Q_PROPERTY(TRange_Chanel_Multimeter* ranges READ getRanges() CONSTANT)
public:

    static TController *Hinstance();
    virtual ~TController();   
    Q_INVOKABLE bool changeRangeChanel(unsigned int aChanel,unsigned int aRange);
    Q_INVOKABLE bool startMeasureChanel(unsigned int aIndex);
    Q_INVOKABLE bool stopMeasureChanel(unsigned int aIndex);
    Q_INVOKABLE bool resultMeasureChanel(unsigned int aIndex);
    Q_INVOKABLE void updateDisplayValue();



private:
    TController();
    TMultimeter_Chanel_Model *getChanels();
    TRange_Chanel_Multimeter *getRanges();
private:
    int FHandleSocketDevice;    
    TRange_Chanel_Multimeter FRange;
    TMultimeter_Chanel_Model FChanels;
    QMutex FMutex;
    bool FClothReadThread;
    QThread FThreadReadStatus;
private:
    static TController* FHandleObjectController;
    static const QString DeviceSocetName;
private slots:
    void ReadStateChanels();
};

#endif // TCONTROLLER_H
