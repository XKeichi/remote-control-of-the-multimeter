#include "tmultimeter_chanel_model.h"
#include <QDebug>
TMultimeter_Chanel_Model::TMultimeter_Chanel_Model(QObject *parent) :
    QAbstractListModel(parent)
{
    FRoles[roleID] = "idchanel";
    FRoles[roleRange] = "range";
    FRoles[roleStatus] = "status";
    FRoles[roleValue] = "value";
}

TMultimeter_Chanel_Model::~TMultimeter_Chanel_Model()
{
    for(auto iChanel=FChanels.begin();iChanel!=FChanels.end();++iChanel)
    {
        delete *iChanel;
        *iChanel = NULL;
    }
    FChanels.clear();
    FRoles.clear();
}

QHash<int,QByteArray>TMultimeter_Chanel_Model::roleNames()const
{
    return FRoles;
}

int TMultimeter_Chanel_Model::rowCount(const QModelIndex &parent) const
{;
    Q_UNUSED(parent)
    return FChanels.size();
}
QVariant TMultimeter_Chanel_Model::data(const QModelIndex &index, int role) const
{    
    int iRow = index.row();

    if(iRow<0||FChanels.size()<=iRow)
    {

        return QVariant();
    }

    switch (role)
    {
    case roleID:
        return QString("Канал-"+QString::number(iRow));
    case roleRange:
        return FChanels[iRow]->Range().isEmpty()?"N/A":FChanels[iRow]->Range();
    case roleStatus:
        return FChanels[iRow]->State();
    case roleValue:
        return FChanels[iRow]->Value().isEmpty()?"N/A":FChanels[iRow]->Value();
    default:;

    }
    return QVariant();
    qDebug()<<"Ошибка роли модели";
    throw QString("Ошибка роли модели");
}

QVector<TChanelStateMultimeter*>& TMultimeter_Chanel_Model::Chanels()
{
    return FChanels;
}

void TMultimeter_Chanel_Model::updateDisplayValue(unsigned int* aIndex)
{
    QModelIndex iStart = (aIndex)?index(*aIndex,0):index(0,0);
    QModelIndex iEnd   = (aIndex)?index(*aIndex,0):index(FChanels.size()-1,0);
    for(int iRole=roleID;iRole<=roleValue;iRole++)
        emit dataChanged(iStart,iEnd,QVector<int>(1,iRole));
}

QString TMultimeter_Chanel_Model::cell(unsigned int aRow, unsigned int aColumn)
{
    aColumn+=Qt::UserRole+1;


    if((qint64)aRow<FChanels.size())
    {

        switch (aColumn)
        {
        case roleID:
            return QString("Канал-"+QString::number(aRow));
        case roleRange:
            return FChanels[(qint64)aRow]->Range();
        case roleStatus:
            return FChanels[(qint64)aRow]->State();
        case roleValue:
            return FChanels[(qint64)aRow]->Value();
        default:;

        }
        qDebug()<<QString("TMultimeter_Chanel_Model::cell()->Ошибка индекса[aColumn]");
        throw(QString("TMultimeter_Chanel_Model::cell()->Ошибка индекса[aColumn]"));
    }
    qDebug()<<QString("TMultimeter_Chanel_Model::cell()->Ошибка индекса[aRow]");
    throw(QString("TMultimeter_Chanel_Model::cell()->Ошибка индекса[aRow]"));

}


