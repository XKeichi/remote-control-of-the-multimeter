#ifndef TMULTIMETER_CHANEL_MODEL_H
#define TMULTIMETER_CHANEL_MODEL_H

#include <QObject>
#include <QVector>
#include <QAbstractListModel>
#include "tchanelstatemultimeter.h"
#include <QDebug>
class TMultimeter_Chanel_Model : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit TMultimeter_Chanel_Model(QObject *parent = 0);
    virtual ~TMultimeter_Chanel_Model();
    virtual int rowCount(const QModelIndex &parent) const override;
    virtual QVariant data(const QModelIndex &index, int role) const override;
    QVector<TChanelStateMultimeter*>& Chanels();
    Q_INVOKABLE void updateDisplayValue(unsigned int* aIndex=NULL);
    Q_INVOKABLE QString cell(unsigned int aRow, unsigned int aColumn);
protected:
    virtual QHash<int,QByteArray>roleNames()const override;
private:   
    enum TRole{roleID=Qt::UserRole+1,roleRange=Qt::UserRole+2,roleStatus=Qt::UserRole+3,roleValue=Qt::UserRole+4};
    QHash<int,QByteArray>FRoles;
    QVector<TChanelStateMultimeter*>FChanels;





};


#endif // TMULTIMETER_CHANEL_MODEL_H
