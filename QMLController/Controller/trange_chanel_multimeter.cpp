#include "trange_chanel_multimeter.h"

TRange_Chanel_Multimeter::TRange_Chanel_Multimeter():QAbstractListModel(NULL)
{
    FRoles[roleDisplay] = "text";
}

TRange_Chanel_Multimeter::~TRange_Chanel_Multimeter()
{
    FRoles.clear();
}

QHash<int,QByteArray>TRange_Chanel_Multimeter::roleNames()const
{
    return FRoles;
}

int TRange_Chanel_Multimeter::rowCount(const QModelIndex &parent) const
{;
    Q_UNUSED(parent)
    return size();
}
QVariant TRange_Chanel_Multimeter::data(const QModelIndex &index, int role) const
{
    int iRow = index.row();

    if(iRow<0||size()<=iRow)
    {

        return QVariant();
    }
    if(role==roleDisplay)
        return at(iRow);

    return QVariant();

}
QString TRange_Chanel_Multimeter::value(unsigned int aIndex)
{
    if(aIndex<(qint64)size())
        return QStringList::operator [](aIndex);
    qDebug()<<"TRange_Chanel_Multimeter[]->Ошибка индекса[aIndex]";
    throw QString("TRange_Chanel_Multimeter[]->Ошибка индекса[aIndex]");
}
