#ifndef TRANGE_CHANEL_MULTIMETER_H
#define TRANGE_CHANEL_MULTIMETER_H
#include <QAbstractListModel>
#include <QStringList>
#include <QDebug>
class TRange_Chanel_Multimeter : public QAbstractListModel, public QStringList
{
    Q_OBJECT
    Q_PROPERTY(unsigned int count READ size CONSTANT)
public:
    TRange_Chanel_Multimeter();
    virtual ~TRange_Chanel_Multimeter();
    virtual int rowCount(const QModelIndex &parent) const override;
    virtual QVariant data(const QModelIndex &index, int role) const override;
    Q_INVOKABLE QString value(unsigned int aIndex);

protected:
    virtual QHash<int,QByteArray>roleNames()const override;
private:
    enum TRole{roleDisplay=Qt::UserRole};
    QHash<int,QByteArray>FRoles;


};

#endif // TRANGE_CHANEL_MULTIMETER_H
