

import QtQuick 2.0
import QtQuick.Controls 1.2
import QtQuick.Dialogs 1.1

import studio.controller.chanels 1.0

import "uiConstants.js" as UIConstants
Row {

    width: parent.width
    height: UIConstants.heightColumnTable()
    function select()
    {
        owner.currentIndex = index
    }


    property ListView owner: null
    property int selectFieldTable: -1
    property int rowIndexTable: -1
    property int selectRangeChanel: 0;

    Rectangle
    {
        width: parent.width/4
        height: parent.height
        border.width: 1
        Text
        {
            id:idChanField
            text: model.idchanel
        }
        MouseArea
        {
            anchors.fill: parent
            onClicked:
            {
                selectFieldTable = 0;
                select()
            }
        }
        color: owner.currentIndex==index?"lightsteelblue":"white"

    }
    Rectangle
    {
        width: parent.width/4
        height: parent.height
        border.width: 1
        id:idRangeField
        Loader
        {
            id: selectWidgetColumn
            sourceComponent:((index==owner.currentIndex)&&(selectFieldTable==1))?(controllRangeField):(displayRangeField)
            anchors.fill: parent
        }
        color: owner.currentIndex==index?"lightsteelblue":"white"
    }
    Rectangle
    {
        width: parent.width/4
        height: parent.height
        border.width: 1
        Text
        {
            id:statusChanelField
            text: model.status
        }
        MouseArea
        {
            anchors.fill: parent
            onClicked:
            {
                selectFieldTable = 2;
                select()
            }
        }
        color: owner.currentIndex==index?"lightsteelblue":"white"

    }
    Rectangle
    {
        width: parent.width/4
        height: parent.height
        border.width: 1
        Text
        {
            id:resultChanelField
            text: model.value
        }
        MouseArea
        {
            anchors.fill: parent
            onClicked:
            {
                selectFieldTable = 3;
                select()
            }
        }
        color: owner.currentIndex==index?"lightsteelblue":"white"


    }

    Component
    {
        id: controllRangeField
        ComboBox
        {

            MessageDialog
            {
                id: messageDialog
                title: "Ошибка"
                text: "Ошибка изменения диаппазона измерения канала мультиметра"
                icon: StandardIcon.Warning

            }
            currentIndex: 0
            id: comboBoxRangeField
            model:Multimeter.ranges
            onActivated:
            {
                if(Multimeter.changeRangeChanel(rowIndexTable,index)===false)
                {
                    messageDialog.visible = true;
                    currentIndex = -1;
                }
                else
                    currentIndex = index;
                selectWidgetColumn.sourceComponent=displayRangeField;
            }
            Component.onCompleted:currentIndex = selectRangeChanel

        }

    }
    Component
    {
        id: displayRangeField
        Text
        {
            id:idRangeField
            text: model.range
            MouseArea
            {
                anchors.fill: parent
                onClicked:
                {
                    rowIndexTable = index
                    if(Multimeter.chanels.cell(index,2)!=="Готов")
                        return;
                    var slectRangeValue = Multimeter.chanels.cell(index,1);
                    if(slectRangeValue==="0-0")
                    {
                        if(Multimeter.changeRangeChanel(rowIndexTable,0)===false)
                            selectRangeChanel = -1;
                    }
                    else
                    {
                        for(var i=0;i<Multimeter.ranges.count;++i)
                        {
                            if(Multimeter.ranges.value(i)===slectRangeValue)
                            {
                                selectRangeChanel = i;
                                break;
                            }
                        }
                    }

                    selectFieldTable = 1;
                    if(owner.currentIndex!==index)
                        select();
                    else
                        selectWidgetColumn.sourceComponent=controllRangeField;
                }
            }
        }
    }

}


