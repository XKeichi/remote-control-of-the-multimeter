import QtQuick 2.0
import "uiConstants.js" as UIConstants

Row {
    Gradient
    {
        id: bagroundColumnHeader
        GradientStop { position: 0.0; color: "white" }
        GradientStop { position: 1.0; color: "silver" }
    }
    width: parent.width
    height: UIConstants.heightColumnTable()
    Rectangle
    {
        width: parent.width/4
        height: parent.height
        border.width: 1
        gradient: bagroundColumnHeader
        Text
        {
            anchors.centerIn: parent
            id:idChanField
            text: "Идентификатор"
        }

    }
    Rectangle
    {
        width: parent.width/4
        height: parent.height
        border.width: 1
        gradient: bagroundColumnHeader
        Text
        {
            id:idRangeField
            anchors.centerIn: parent
            text: "Диапазон измерения"
        }

    }
    Rectangle
    {
        width: parent.width/4
        height: parent.height
        border.width: 1
        gradient: bagroundColumnHeader
        Text
        {
            id:statusChanelField
            anchors.centerIn: parent
            text: "Статус"
        }

    }
    Rectangle
    {
        width: parent.width/4
        height: parent.height
        border.width: 1
        gradient: bagroundColumnHeader
        Text
        {
            id:resultChanelField
            anchors.centerIn: parent
            text: "Значение"
        }

    }


}


