import QtQuick 2.0
import studio.controller.chanels 1.0
import "uiConstants.js" as UI
Rectangle
{
    property alias selectChanelMultimeter: tableChanelMultimetr.currentIndex

    HeaderTableMultimetrs
    {
        anchors.left: parent.left
        anchors.right: parent.right;
        anchors.top: parent.top
        id: header
    }

    Component
    {
        id: uiSelectionRowTable
        Rectangle
        {
            width: ListView.width
            color: "lightsteelblue"
        }
    }
    Component
    {
        id: delegateCellTable
        Loader
        {
            id: fieldsMultimetrTable
            source: "DelegateMultimeterFields.qml"
            width: parent.width
            onLoaded:
            {
                item.owner = tableChanelMultimetr
            }

        }

    }

    ListView
    {
        anchors.left: parent.left
        anchors.right: parent.right;
        anchors.top: header.bottom
        anchors.bottom: parent.bottom        
        currentIndex: -1
        clip: true
        id: tableChanelMultimetr

        delegate: delegateCellTable

        model: Multimeter.chanels


        highlight: uiSelectionRowTable


    }


}





