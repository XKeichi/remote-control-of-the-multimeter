#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QtGui>
#include <QtQml>
#include <memory>
#include "Controller/tcontroller.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);


    QQmlApplicationEngine engine;
    try
    {
        std::shared_ptr<TController>pController(TController::Hinstance());
        TController* Sender = &(*pController);
        engine.rootContext()->setContextProperty("Multimeter",Sender);
        qmlRegisterType<TMultimeter_Chanel_Model>("studio.controller.chanels",1,0,"TMultimeter_Chanel_Model");
        qmlRegisterType<TRange_Chanel_Multimeter>("studio.controller.range"  ,1,0,"TRange_Chanel_Multimeter");
        engine.load(QUrl(QStringLiteral("qrc:/MainWindowMultimetr/main.qml")));
        return app.exec();
    }
    catch(QString Error)
    {
        qDebug()<<"Error:="<<Error;
    }
    return -1;


}
