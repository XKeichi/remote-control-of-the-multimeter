import QtQuick 2.2
import QtQuick.Window 2.1
import QtQuick.Controls 1.2

Window {
    id: mainWindowConroller
    visible: true
    width: 620
    height: 360

    ToolBar
    {
        id: mainToolBar
        height: 42
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.top: parent.top
        anchors.topMargin: 0

        ToolButton
        {
            id: toolButtonClose
            x: 108
            y: 6
            text: "Выход"
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: parent.right
            anchors.rightMargin: 0
            onClicked: Qt.quit()

        }



        ToolButton
        {
            id: toolButtonStartmesure
            y: 7
            height: 25
            text: "Запуск измерения"
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
            anchors.leftMargin: 0
            onClicked:
            {
                if(Multimeter.startMeasureChanel(tableChanelMultimetr.selectChanelMultimeter))
                {
                    Multimeter.updateDisplayValue();
                }
            }
        }

        ToolButton
        {
            id: toolButtonStopMesure
            y: 7
            text: "Отключение измерения"
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: toolButtonStartmesure.right
            anchors.leftMargin: 3
            onClicked:
            {
                if(Multimeter.stopMeasureChanel(tableChanelMultimetr.selectChanelMultimeter))
                {
                    Multimeter.updateDisplayValue();
                }
            }

        }

        ToolButton
        {
            id: toolButtonResultMesureValue
            y: 7
            text: "Получить результат канала"
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: toolButtonStopMesure.right
            anchors.leftMargin: 3
            onClicked:
            {
                if(Multimeter.resultMeasureChanel(tableChanelMultimetr.selectChanelMultimeter))
                {
                    Multimeter.updateDisplayValue();
                }

            }
        }
    }

    TableChanelMultimetr
    {
        id: tableChanelMultimetr
        anchors.top: mainToolBar.bottom
        anchors.topMargin: 0
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.left: parent.left
    }

}
